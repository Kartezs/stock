package ru.kartezs.stock.service;

import lombok.NonNull;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.Product;
import ru.kartezs.stock.domains.Stock;
import ru.kartezs.stock.dto.DemandDTO;
import ru.kartezs.stock.dto.PurchaseDTO;
import ru.kartezs.stock.exceptions.FrontException;
import ru.kartezs.stock.repo.StockRepo;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class StockService {

    private StockRepo repo;
    private ProductService productService;

    public StockService(Connection connection) {
        this.repo = new StockRepo(connection);
        this.productService = new ProductService(connection);
    }

    public StockService(StockRepo repo, ProductService productService) {
        this.repo = repo;
        this.productService = productService;
    }

    /**
     * Create stocks from given PurchaseDTO. Stocks Count depends on PurchaseDTO.count variable.
     * @param dto
     * @throws SQLException
     * @throws FrontException
     */
    public void createStocksFromPurchase(@NonNull PurchaseDTO dto) throws SQLException, FrontException {
        Product product = productService.getProduct(dto.getProductName());

        int stocksCount = dto.getCount();
        if (stocksCount < 0){
            throw new IllegalArgumentException("Count must be > 0");
        }

        for (int i = 0; i < stocksCount; i++) {
            repo.createPurchase(createPurchaseStock(product.getId(), dto.getBuyDate(), dto.getBuyPrice()));
        }
    }

    /**
     * Update stocks(created by purchase) from given DemandDTO. Updated Stocks Count depends on DemandDTO.count variable.
     *
     * If updateStocksByFIFO(...) return EMPTY_PRODUCT_COUNT, it means that demand product count is more than products in db.
     * In this case method will throw FrontException EMPTY_PRODUCT_COUNT and count of available stocks to update
     *
     * If updateStocksByFIFO(...) return SOLD_DATE_EXC, it means that demand product soldDate before than buyDate.
     * In this case method will throw FrontException SOLD_DATE_EXC and count of available stocks to update
     * @param dto
     * @throws SQLException
     * @throws FrontException
     */
    public void updateStocksFromDemand(DemandDTO dto) throws SQLException, FrontException {
        Product product = productService.getProduct(dto.getProductName());

        int stocksCount = dto.getCount();
        if (stocksCount < 0){
            throw new IllegalArgumentException("Count must be > 0");
        }

        int unAvailableProductCount = 0;
        StockRepo.ExceptionType result = null;
        for (int i = 0; i < stocksCount; i++){
            result = repo.updateStocksByFIFO(createDemandStock(product.getId(), dto.getSoldDate(), dto.getSoldPrice()));
            if (result != null){
                unAvailableProductCount++;
            }
        }
        if (unAvailableProductCount > 0){
            switch (result){
                case EMPTY_PRODUCT_COUNT: throw new FrontException(FrontAnswer.EMPTY_PRODUCT_COUNT.updateErrorMessage(String.valueOf(stocksCount - unAvailableProductCount)));
                case SOLD_DATE_EXC: throw new FrontException(FrontAnswer.SOLD_DATE_EXC.updateErrorMessage(String.valueOf(stocksCount - unAvailableProductCount)));
            }
        }
    }

    /**
     * Return filtered stocks by soldDate(soldDateFromDb <= soldDate) and productName
     * @param date
     * @param productName
     * @return
     * @throws SQLException
     * @throws FrontException
     */
    public List<Stock> getSoldByDataAndProductName(LocalDate date, String productName) throws SQLException, FrontException {
        Product product = productService.getProduct(productName);
        return repo.findBySoldDateAndProductId(date, product.getId());
    }

    Stock createPurchaseStock(String productId, LocalDate buyDate, double buyPrice){
        Stock stock = new Stock();
        stock.setId(UUID.randomUUID().toString());
        stock.setProductId(productId);
        stock.setBuyDate(buyDate);
        stock.setBuyPrice(buyPrice);
        return stock;
    }

    Stock createDemandStock(String productId, LocalDate soldDate, double soldPrice){
        Stock stock = new Stock();
        stock.setProductId(productId);
        stock.setSoldDate(soldDate);
        stock.setSoldPrice(soldPrice);
        stock.setSold(true);
        return stock;
    }
}
