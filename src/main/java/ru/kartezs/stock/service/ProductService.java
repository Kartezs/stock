package ru.kartezs.stock.service;

import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.Product;
import ru.kartezs.stock.repo.ProductRepo;
import ru.kartezs.stock.exceptions.FrontException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

public class ProductService {

    private ProductRepo repo;

    public ProductService(Connection connection) {
        this.repo = new ProductRepo(connection);
    }

    public ProductService(ProductRepo repo) {
        this.repo = repo;
    }

    /**
     * If product with given name is not exist, method will generate id, save and return new product.
     * If product with given name is exist, method will return null.
     * @param name - product name
     * @return
     * @throws SQLException
     * @throws FrontException - if given name is empty or null
     */
    public Product createProduct(String name) throws SQLException, FrontException {
        if (name == null || name.isEmpty()){
            throw new FrontException(FrontAnswer.WRONG_PRODUCT_NAME);
        }
        if (!repo.isProductExist(name)) {
            return repo.saveProduct(createNewProduct(name));
        } else {
            return null;
        }
    }

    /**
     * Return product from db by name. If product don't exist, method will return new product with given name.
     * @param name - product name
     * @return
     * @throws FrontException - if name is null or empty
     * @throws SQLException
     */
    public Product getProduct(String name) throws FrontException, SQLException {
        if (name == null || name.isEmpty()){
            throw new FrontException(FrontAnswer.WRONG_PRODUCT_NAME);
        }
        Product productFromDb =  repo.getProductByName(name);
        if (productFromDb == null) {
            return repo.saveProduct(createNewProduct(name));
        } else {
            return productFromDb;
        }
    }

    Product createNewProduct(String name){
        return new Product(UUID.randomUUID().toString(), name);
    }

}
