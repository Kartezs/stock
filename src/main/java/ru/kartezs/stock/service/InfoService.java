package ru.kartezs.stock.service;

import ru.kartezs.stock.domains.Stock;
import ru.kartezs.stock.dto.SalesReportDTO;
import ru.kartezs.stock.exceptions.FrontException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class InfoService {

    private StockService stockService;

    public InfoService(Connection connection) {
        this.stockService = new StockService(connection);
    }

    public double calculateProfit(SalesReportDTO jsonObject) throws SQLException, FrontException {
        List<Stock> stocks = stockService.getSoldByDataAndProductName(jsonObject.getDate(), jsonObject.getProductName());
        return stocks.stream().mapToDouble(stock -> stock.getSoldPrice() - stock.getBuyPrice()).sum();
    }
}
