package ru.kartezs.stock.exceptions;

public class DataException extends Exception {

    public DataException(String message) {
        super(message);
    }
}
