package ru.kartezs.stock.exceptions;

import ru.kartezs.stock.domains.CustomError;

public class FrontException extends Exception {

    private CustomError customError;

    public FrontException(CustomError customError) {
        this.customError = customError;
    }

    public CustomError getCustomError() {
        return customError;
    }
}
