package ru.kartezs.stock.exceptions;

public class CustomServletException extends Exception{
    public CustomServletException(String message) {
        super(message);
    }
}
