package ru.kartezs.stock.exceptions;

public class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }
}
