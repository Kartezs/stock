package ru.kartezs.stock.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.CheckData;
import ru.kartezs.stock.domains.LocalDateDeserializer;
import ru.kartezs.stock.domains.LocalDateSerializer;
import ru.kartezs.stock.exceptions.FrontException;

import java.time.LocalDate;

@Getter
@Setter
public class SalesReportDTO implements CheckData<SalesReportDTO> {

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate date;
    private String productName;

    public SalesReportDTO(){}

    public SalesReportDTO(LocalDate date, String productName) {
        this.date = date;
        this.productName = productName;
    }

    @Override
    public void check() throws FrontException {
        if (productName == null || productName.isEmpty()){
            throw new FrontException(FrontAnswer.WRONG_PRODUCT_NAME);
        }
        if (date == null){
            throw new FrontException(FrontAnswer.WRONG_DEMAND_DATE);
        }
    }

    @Override
    public SalesReportDTO getData() {
        return this;
    }
}
