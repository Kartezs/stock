package ru.kartezs.stock.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.CheckData;
import ru.kartezs.stock.domains.LocalDateDeserializer;
import ru.kartezs.stock.domains.LocalDateSerializer;
import ru.kartezs.stock.exceptions.FrontException;

import java.time.LocalDate;

@Getter
@Setter
public class DemandDTO implements CheckData<DemandDTO> {
    private String productName;
    private int count;
    private double soldPrice;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate soldDate;


    @Override
    public void check() throws FrontException {
        if (count < 0){
            throw new FrontException(FrontAnswer.WRONG_DEMAND_COUNT);
        }
        if (soldPrice <= 0){
            throw new FrontException(FrontAnswer.WRONG_DEMAND_SOLDPRICE);
        }
        if (productName == null || productName.isEmpty()){
            throw new FrontException(FrontAnswer.WRONG_PRODUCT_NAME);
        }
        if (soldDate == null){
            throw new FrontException(FrontAnswer.WRONG_DEMAND_DATE);
        }
    }

    @Override
    public DemandDTO getData() {
        return this;
    }
}
