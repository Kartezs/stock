package ru.kartezs.stock.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.CheckData;
import ru.kartezs.stock.domains.LocalDateDeserializer;
import ru.kartezs.stock.domains.LocalDateSerializer;
import ru.kartezs.stock.exceptions.FrontException;

import java.time.LocalDate;

@Getter
@Setter
public class PurchaseDTO implements CheckData<PurchaseDTO> {
    private String productName;
    private int count;
    private double buyPrice;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate buyDate;

    @Override
    public void check() throws FrontException {
        if (count < 0){
            throw new FrontException(FrontAnswer.WRONG_PURCHASE_COUNT);
        }
        if (buyPrice <= 0){
            throw new FrontException(FrontAnswer.WRONG_PURCHASE_BUYPRICE);
        }
        if (productName == null || productName.isEmpty()){
            throw new FrontException(FrontAnswer.WRONG_PRODUCT_NAME);
        }
        if (buyDate == null){
            throw new FrontException(FrontAnswer.WRONG_PURCHASE_DATE);
        }
    }

    @Override
    public PurchaseDTO getData() {
        return this;
    }
}
