package ru.kartezs.stock;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationConfig {

    private static final Logger log = LogManager.getLogger(ApplicationConfig.class);
    private static ApplicationConfig applicationConfig;
    // Path to config file
    public static final String CONFIG_FILE_PATH = "config.path";
    public static final String DB_URL = "db.url";
    public static final String DB_USER = "db.user";
    public static final String DB_PASS = "db.pass";
    // Path to start sql script
    public static final String START_SQL_SCRIPT_PATH = "start.sql.path";
    // SCHEMA name
    public static final String SCHEMA = "db.schema";

    private static final String DEFAULT_CONFIG_FILE = "application.properties";
    public static final String DEFAULT_SQL_SCRIPT_NAME = "start.sql";


    public static ApplicationConfig getInstance() {
        if (applicationConfig == null) {
            applicationConfig = new ApplicationConfig();
        }
        return applicationConfig;
    }

    public static String getProperty(String name) {
        ApplicationConfig config = ApplicationConfig.getInstance();
        return config.getProperties().getProperty(name);
    }

    @Getter
    private Properties properties;
    private Properties defaultProperties;

    private ApplicationConfig() {
        init();
    }

    private void init(){
        log.info("Start reading config properties");
        properties = new Properties();
        defaultProperties = new Properties();

        // load default properties
        loadProperties(getDefaultConfigFile(), defaultProperties);

        File customConfigFile;
        String configFilePath = System.getProperty(CONFIG_FILE_PATH);

        // try to load custom properties
        if (configFilePath != null && !configFilePath.isEmpty()) {
            customConfigFile = new File(configFilePath);
            if (customConfigFile.exists() && !customConfigFile.isDirectory()) {
                loadProperties(customConfigFile, properties);
            }
        }

        // if custom properties is not set
        if (properties.isEmpty()){
            properties = defaultProperties;
            // If using default config, START_SQL_SCRIPT_PATH will be load from classpath
            log.info("Unable to find config file path, -Dconfig.path is not set or incorrect. Using default properties.");
        }

        checkProperties(properties);

        log.info("Config properties: " + properties);
    }

    private void checkProperties(Properties properties){
        // Check schema name
        String schema = properties.getProperty(SCHEMA);
        if (schema == null || schema.isEmpty()) {
            properties.setProperty(SCHEMA, defaultProperties.getProperty(SCHEMA));
            log.warn("db.schema is not set in config file. Using default schema name");
        }
        // Check start sql script
        String startSqlPath = properties.getProperty(START_SQL_SCRIPT_PATH);
        if (startSqlPath == null || startSqlPath.isEmpty()) {
            properties.setProperty(START_SQL_SCRIPT_PATH, this.getClass().getClassLoader().getResource(DEFAULT_SQL_SCRIPT_NAME).getPath());
            log.warn("start.sql.path is not set in config file. Using default start sql file");
        }
    }

    private File getDefaultConfigFile() {
        return new File(this.getClass().getClassLoader().getResource(DEFAULT_CONFIG_FILE).getPath());
    }

    private void loadProperties(File file, Properties properties) {
        try (InputStream inputStream = new FileInputStream(file)) {
            properties.load(inputStream);
        } catch (IOException e) {
            log.error("Unable to read application properties", e);
        }
    }

}
