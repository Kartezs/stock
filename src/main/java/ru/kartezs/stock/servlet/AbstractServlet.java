package ru.kartezs.stock.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.Utils;
import ru.kartezs.stock.exceptions.FrontException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AbstractServlet set default setting and write frontend result
 */
public abstract class AbstractServlet extends HttpServlet {

    private static final Logger log = LogManager.getLogger(AbstractServlet.class);

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setResponseSettings(response);
        try {
            if (!request.getContentType().equalsIgnoreCase("application/json")) {
                throw new FrontException(FrontAnswer.WRONG_REQUEST_BODY_TYPE);
            }
            runInternal(request, response);
            String successMessage = getSuccessMessage();
            log.info("Answer: " + successMessage);
            response.getWriter().println(successMessage);
        } catch (Exception e) {
            FrontException frontException = Utils.findException(e, FrontException.class);
            if (frontException == null){
                log.error("Server error", e);
            }
            writeFrontendExceptionToResponse(response, frontException);
        }
    }

    protected abstract void runInternal(HttpServletRequest request, HttpServletResponse response) throws Exception;

    protected abstract String getSuccessMessage();

    protected void writeFrontendExceptionToResponse(HttpServletResponse response, FrontException frontException) throws IOException {
        String errorAnswer;
        if (frontException == null){
            errorAnswer = FrontAnswer.INTERNAL_SERVER_ERROR.toString();
        } else {
            errorAnswer = frontException.getCustomError().toString();
        }
        log.info("Frontend error answer: " + errorAnswer);
        response.getWriter().println(errorAnswer);
    }

    protected void setResponseSettings(HttpServletResponse response){
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
    }
}
