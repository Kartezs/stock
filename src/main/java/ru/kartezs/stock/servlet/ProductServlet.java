package ru.kartezs.stock.servlet;

import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.Product;
import ru.kartezs.stock.exceptions.FrontException;
import ru.kartezs.stock.service.ProductService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;

@WebServlet(urlPatterns = "/newproduct")
public class ProductServlet extends AbstractConnectionServlet<Product> {

    @Override
    protected void internalServletCall(HttpServletRequest request, HttpServletResponse response, Connection connection, Product jsonObject) throws Exception {
        if (request.getMethod().equals("POST")) {
            Product product = new ProductService(connection).createProduct(jsonObject.getName());
            if (product == null){
                throw new FrontException(FrontAnswer.PRODUCT_EXIST);
            }
        } else {
            throw new FrontException(FrontAnswer.METHOD_IS_UNSUPPORTED);
        }
    }

    @Override
    protected Product getRequestDTO() {
        return new Product();
    }

    @Override
    protected String getSuccessMessage() {
        return FrontAnswer.OK;
    }
}
