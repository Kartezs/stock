package ru.kartezs.stock.servlet;

import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.dto.DemandDTO;
import ru.kartezs.stock.exceptions.FrontException;
import ru.kartezs.stock.service.StockService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;

@WebServlet(urlPatterns = "/demand")
public class DemandServlet extends AbstractConnectionServlet<DemandDTO> {

    @Override
    protected void internalServletCall(HttpServletRequest request, HttpServletResponse response, Connection connection, DemandDTO jsonObject) throws Exception {
        if (request.getMethod().equals("POST")) {
            new StockService(connection).updateStocksFromDemand(jsonObject);
        } else {
            throw new FrontException(FrontAnswer.METHOD_IS_UNSUPPORTED);
        }
    }

    @Override
    protected DemandDTO getRequestDTO() {
        return new DemandDTO();
    }

    @Override
    protected String getSuccessMessage() {
        return FrontAnswer.OK;
    }
}