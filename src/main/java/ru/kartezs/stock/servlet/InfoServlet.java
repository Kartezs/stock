package ru.kartezs.stock.servlet;

import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.dto.SalesReportDTO;
import ru.kartezs.stock.exceptions.FrontException;
import ru.kartezs.stock.service.InfoService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;

@WebServlet(urlPatterns = "/salesreport")
public class InfoServlet extends AbstractConnectionServlet<SalesReportDTO> {

    private double profit;

    @Override
    protected void internalServletCall(HttpServletRequest request, HttpServletResponse response, Connection connection, SalesReportDTO jsonObject) throws Exception {
        if (request.getMethod().equals("GET")) {
            profit = new InfoService(connection).calculateProfit(jsonObject);
        } else {
            throw new FrontException(FrontAnswer.METHOD_IS_UNSUPPORTED);
        }
    }

    @Override
    protected SalesReportDTO getRequestDTO() {
        return new SalesReportDTO();
    }

    @Override
    protected String getSuccessMessage() {
        return String.format("{\"profit\": %s }", String.format("%.2f",profit).replaceAll(",","."));
    }
}
