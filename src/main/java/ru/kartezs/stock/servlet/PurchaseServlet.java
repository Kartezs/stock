package ru.kartezs.stock.servlet;

import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.dto.PurchaseDTO;
import ru.kartezs.stock.exceptions.FrontException;
import ru.kartezs.stock.service.StockService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;

@WebServlet(urlPatterns = "/purchase")
public class PurchaseServlet extends AbstractConnectionServlet<PurchaseDTO> {

    @Override
    protected void internalServletCall(HttpServletRequest request, HttpServletResponse response, Connection connection, PurchaseDTO jsonObject) throws Exception {
        if (request.getMethod().equals("POST")) {
            new StockService(connection).createStocksFromPurchase(jsonObject);
        } else {
            throw new FrontException(FrontAnswer.METHOD_IS_UNSUPPORTED);
        }
    }

    @Override
    protected PurchaseDTO getRequestDTO() {
        return new PurchaseDTO();
    }

    @Override
    protected String getSuccessMessage() {
        return FrontAnswer.OK;
    }
}