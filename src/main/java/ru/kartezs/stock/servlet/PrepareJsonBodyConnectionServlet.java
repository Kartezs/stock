package ru.kartezs.stock.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kartezs.stock.Utils;
import ru.kartezs.stock.domains.CheckData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * PrepareJsonBodyConnectionServlet mapped request body to given DTO object.
 * @param <T>
 */
public abstract class PrepareJsonBodyConnectionServlet<T extends CheckData> extends AbstractServlet {

    private static final Logger log = LogManager.getLogger(PrepareJsonBodyConnectionServlet.class);

    @Override
    protected void runInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CheckData<T> checkData = Utils.requestBodyToObject(readRequestContent(request), getRequestDTO());
        checkData.check();
        internalCallWithJsonBody(request, response, checkData.getData());
    }

    protected abstract void internalCallWithJsonBody(HttpServletRequest request, HttpServletResponse response, T jsonObject) throws Exception;

    protected abstract T getRequestDTO();

    String readRequestContent(HttpServletRequest request) throws IOException {
        StringBuilder content = new StringBuilder();
        log.info("------ REQUEST ------");
        log.info(request.getMethod() + " " + request.getRequestURL());

        Enumeration<String> headersNames = request.getHeaderNames();
        while (headersNames.hasMoreElements()) {
            String headerName = headersNames.nextElement();
            Enumeration<String> headers = request.getHeaders(headerName);
            while (headers.hasMoreElements()) {
                String headerValue = headers.nextElement();
                log.info(headerName + ": " + headerValue);
            }

        }
        log.info("------ CONTENT ------");
        request.getReader().lines().forEach(s -> {
            content.append(s);
            log.info(s);
        });
        log.info("---------------------");

        return content.toString();
    }
}
