package ru.kartezs.stock.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.Utils;
import ru.kartezs.stock.db.Connector;
import ru.kartezs.stock.domains.CheckData;
import ru.kartezs.stock.exceptions.FrontException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class AbstractConnectionServlet<T extends CheckData> extends PrepareJsonBodyConnectionServlet<T> {

    private static final Logger log = LogManager.getLogger(AbstractConnectionServlet.class);

    private Connector connector;

    @Override
    public void init() throws ServletException {
        connector = (Connector) getServletContext().getAttribute("connector");
    }

    @Override
    protected void internalCallWithJsonBody(HttpServletRequest request, HttpServletResponse response, T jsonObject) throws Exception {
            Connection connection = null;
            try {
                connection = getConnection();
                internalServletCall(request, response, connection, jsonObject);
            } catch (Exception ex) {
                if (connection != null) {
                    connection.rollback();
                }
                Exception sqlException = Utils.findException(ex, SQLException.class);
                if (sqlException != null){
                    log.error("DB work exception", sqlException);
                    throw new FrontException(FrontAnswer.INTERNAL_SERVER_ERROR);
                } else {
                    throw ex;
                }
            } finally {
                try {
                    if (connection != null) {
                        connection.commit();
                        connection.close();
                    }
                } catch (SQLException e){
                    log.error("Unable close connection", e);
                }
            }
    }


    protected abstract void internalServletCall(HttpServletRequest request, HttpServletResponse response, Connection connection, T jsonObject) throws Exception;

    Connection getConnection() throws SQLException {
        return connector.getConnection();
    }

}
