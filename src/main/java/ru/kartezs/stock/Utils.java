package ru.kartezs.stock;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kartezs.stock.exceptions.FrontException;

import java.io.IOException;

public class Utils {

    private static final Logger log = LogManager.getLogger(Utils.class);
    /**
     * Util method which mapping request body to given DTO object
     * @param content
     * @param object
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T requestBodyToObject(String content, T object) throws FrontException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return (T) mapper.readValue(content, object.getClass());
        }catch (Exception e){
            FrontException frontException = findException(e, FrontException.class);
            if (frontException != null){
                throw frontException;
            }
            log.error("Unable to convert request body to object", e);
            throw new FrontException(FrontAnswer.WRONG_REQUEST_BODY);
        }
    }

    /**
     * Util method which find in given exception target exception
     * If no target exception is found, method will return null
     * @param exception
     * @param targetException
     * @param <T>
     * @return
     */
    public static <T> T findException(Throwable exception, Class<T> targetException){
        if (exception.getClass() == targetException){
            return (T) exception;
        } else {
            if (exception.getCause() != null){
                return findException(exception.getCause(), targetException);
            } else {
                return null;
            }
        }
    }
}
