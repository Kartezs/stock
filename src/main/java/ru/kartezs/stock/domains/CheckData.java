package ru.kartezs.stock.domains;

import ru.kartezs.stock.exceptions.FrontException;

public interface CheckData<T> {

    /**
     * Check correct DTO properties
     * @throws FrontException
     */
    void check() throws FrontException;

    T getData();
}
