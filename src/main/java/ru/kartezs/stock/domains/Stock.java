package ru.kartezs.stock.domains;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@EqualsAndHashCode
public class Stock {

    private String id;
    private String productId;
    private double buyPrice;
    private double soldPrice;
    private LocalDate buyDate;
    private LocalDate soldDate;
    private boolean isSold;
}
