package ru.kartezs.stock.domains;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

import java.time.LocalDate;

public class LocalDateDeserializer extends StdDeserializer<LocalDate> {

    private static final long serialVersionUID = 1L;

    protected LocalDateDeserializer() {
        super(LocalDate.class);
    }

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws JsonProcessingException {
        try {
            return LocalDate.parse(jsonParser.readValueAs(String.class));
        } catch (Exception e){
            throw new JsonParseException(jsonParser, "Unable to parse date", new FrontException(FrontAnswer.WRONG_DATE_FORMAT));
        }
    }
}
