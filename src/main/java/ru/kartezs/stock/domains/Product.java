package ru.kartezs.stock.domains;

import lombok.Getter;
import lombok.Setter;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

import java.util.Objects;

@Getter
@Setter
public class Product implements CheckData<Product> {

    private String id;
    private String name;

    public Product(){}

    public Product(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return id.equals(product.id) &&
                name.equals(product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public void check() throws FrontException {
        if (name == null || name.isEmpty()) {
            throw new FrontException(FrontAnswer.WRONG_PRODUCT_NAME);
        }
    }

    @Override
    public Product getData() {
        return this;
    }
}
