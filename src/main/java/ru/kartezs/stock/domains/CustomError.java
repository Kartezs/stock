package ru.kartezs.stock.domains;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class CustomError {

    private int code;
    private String errorMessage;
    private int httpError;

    public CustomError(int code, String errorMessage, int httpError) {
        this.code = code;
        this.errorMessage = errorMessage;
        this.httpError = httpError;
    }

    public CustomError updateErrorMessage(String... strings){
        return new CustomError(this.code, String.format(errorMessage, strings), this.httpError);
    }

    /**
     * Return error json by code and message
     * @return
     */
    @Override
    public String toString() {
        return String.format("{\"code\": %d, \"errorMessage\": \"%s\"}", code, errorMessage);
    }
}
