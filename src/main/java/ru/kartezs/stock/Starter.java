package ru.kartezs.stock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.Driver;
import ru.kartezs.stock.db.Connector;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Starter implements ServletContextListener {

    private static final Logger log = LogManager.getLogger(Starter.class);

    /**
     * Prepare application environment
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            log.info("Start prepare environment");
            ApplicationConfig.getInstance();

            Driver.load();
            Connector connector = new Connector();
            connector.init();

            ServletContext context = servletContextEvent.getServletContext();
            context.setAttribute("connector", connector);
            log.info("Application successfully started!");
        } catch (Exception e) {
            log.error("Unable to create db!", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Application stopped");
    }
}
