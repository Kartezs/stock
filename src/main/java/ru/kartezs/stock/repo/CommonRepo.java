package ru.kartezs.stock.repo;

import ru.kartezs.stock.ApplicationConfig;

import java.sql.Connection;

public class CommonRepo {

    protected String schema;
    protected Connection connection;

    public CommonRepo(Connection connection){
        this.connection = connection;
        this.schema = getSchemaName();
    }

    protected String getSchemaName(){
        return ApplicationConfig.getProperty(ApplicationConfig.SCHEMA);
    }
}
