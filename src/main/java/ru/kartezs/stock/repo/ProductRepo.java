package ru.kartezs.stock.repo;

import ru.kartezs.stock.domains.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductRepo extends CommonRepo {

    public ProductRepo(Connection connection) {
        super(connection);
    }

    public Product saveProduct(Product product) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(String.format("INSERT INTO %s.product VALUES (?, ?)", schema))) {
            statement.setString(1, product.getId());
            statement.setString(2, product.getName());
            statement.execute();
        }
        return product;
    }

    public boolean isProductExist(String name) throws SQLException {
        return getProductByName(name) != null;
    }

    public Product getProductByName(String name) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(String.format("SELECT * FROM %s.product WHERE name = ?", schema))) {
            statement.setString(1, name);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                if (resultSet.next()){
                    return createProduct(resultSet);
                } else {
                    return null;
                }
            }
        }
    }

    private Product createProduct(ResultSet resultSet) throws SQLException {
        Product product = new Product();
        product.setId(resultSet.getString("id"));
        product.setName(resultSet.getString("name"));
        return product;
    }
}
