package ru.kartezs.stock.repo;

import ru.kartezs.stock.domains.Stock;
import ru.kartezs.stock.exceptions.FrontException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class StockRepo extends CommonRepo {

    public enum ExceptionType {
        EMPTY_PRODUCT_COUNT, SOLD_DATE_EXC
    }

    public StockRepo(Connection connection) {
        super(connection);
    }

    public void createPurchase(Stock stock) throws SQLException {
        try (PreparedStatement statement = connection
                .prepareStatement(
                        String.format("INSERT INTO %s.stock(id, productId, buyPrice, buyDate, isSold) VALUES (?, ?, ?, ?, ?)", schema)
                )
        ) {
            statement.setString(1, stock.getId());
            statement.setString(2, stock.getProductId());
            statement.setDouble(3, stock.getBuyPrice());
            statement.setDate(4, Date.valueOf(stock.getBuyDate()));
            statement.setBoolean(5, stock.isSold());
            statement.execute();
        }
    }

    /**
     * Update stock
     *
     * if given soldDate before buyDate, method will return SOLD_DATE_EXC
     * if resultSet is empty, method will return EMPTY_PRODUCT_COUNT
     * if everything ok, method will return null
     * @param stock
     * @return
     * @throws SQLException
     * @throws FrontException
     */
    public ExceptionType updateStocksByFIFO(Stock stock) throws SQLException, FrontException {
        try (PreparedStatement statement = connection
                .prepareStatement(
                        String.format("SELECT TOP 1 * FROM %s.stock WHERE productId = ? and isSold = ? ORDER BY buyDate", schema))
        ) {
            statement.setString(1, stock.getProductId());
            statement.setBoolean(2, !stock.isSold());
            try (ResultSet resultSet = statement.executeQuery()){
                if (resultSet.next()) {
                    String id = resultSet.getString("id");
                    LocalDate buyDate = resultSet.getDate("buyDate").toLocalDate();
                    if (buyDate.isAfter(stock.getSoldDate())){
                        return ExceptionType.SOLD_DATE_EXC;
                    }
                    try(PreparedStatement updateStatement = connection
                            .prepareStatement(
                                    String.format("UPDATE %s.stock SET soldDate = ?, soldPrice = ?, isSold = ? WHERE id = ?", schema))) {
                        updateStatement.setDate(1, Date.valueOf(stock.getSoldDate()));
                        updateStatement.setDouble(2, stock.getSoldPrice());
                        updateStatement.setBoolean(3, stock.isSold());
                        updateStatement.setString(4, id);
                        updateStatement.execute();
                        return null;
                    }
                } else {
                    return ExceptionType.EMPTY_PRODUCT_COUNT;
                }
            }
        }
    }

    public List<Stock> findBySoldDateAndProductId(LocalDate soldDate, String productId) throws SQLException {
        List<Stock> stocks = new ArrayList<>();
        try (PreparedStatement statement = connection
                .prepareStatement(
                        String.format("SELECT * FROM %s.stock WHERE soldDate <= ? AND productId = ? AND isSold = ?", schema)
                )
        ) {
            statement.setDate(1, Date.valueOf(soldDate));
            statement.setString(2, productId);
            statement.setBoolean(3, true);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()){
                while (resultSet.next()){
                    Stock stock = new Stock();
                    stock.setSoldPrice(resultSet.getDouble("soldPrice"));
                    stock.setSoldDate(resultSet.getDate("soldDate").toLocalDate());
                    stock.setBuyPrice(resultSet.getDouble("buyPrice"));
                    stock.setBuyDate(resultSet.getDate("buyDate").toLocalDate());
                    stock.setProductId(resultSet.getString("productId"));
                    stock.setSold(resultSet.getBoolean("isSold"));
                    stock.setId(resultSet.getString("id"));
                    stocks.add(stock);
                }
            }
        }
        return stocks;
    }
}
