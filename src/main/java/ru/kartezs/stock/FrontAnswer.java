package ru.kartezs.stock;

import ru.kartezs.stock.domains.CustomError;

import javax.servlet.http.HttpServletResponse;

public class FrontAnswer {

    public static final CustomError WRONG_REQUEST_BODY_TYPE =
            new CustomError(1,"Тип данных не поддерживается. Используйте JSON.", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError INTERNAL_SERVER_ERROR =
            new CustomError(2,"Внутренняя ошибка сервера", HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    public static final CustomError WRONG_REQUEST_BODY =
            new CustomError(3,"Ошибка обработки запроса. Проверьте корректность отправляемого json-a.", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError WRONG_PRODUCT_NAME =
            new CustomError(4,"Неправильное название продукта", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError PRODUCT_EXIST =
            new CustomError(5,"Продукт с таким именем уже существует", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError METHOD_IS_UNSUPPORTED =
            new CustomError(6,"Данный http метод не поддерживается", HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    public static final CustomError WRONG_PURCHASE_COUNT =
            new CustomError(7,"Количество закупки не может быть нулевым или отрицательным", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError WRONG_PURCHASE_BUYPRICE =
            new CustomError(8,"Цена закупки не может быть нулевой или отрицательной", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError WRONG_PURCHASE_DATE =
            new CustomError(9,"Дата закупки не указана", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError EMPTY_PRODUCT_COUNT =
            new CustomError(10,"Продажа не возможна, недостаточно товара. Доступно %s.", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError WRONG_DATE_FORMAT =
            new CustomError(11,"Не корректный формат даты. Требуемый формат даты: yyyy-mm-dd.", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError WRONG_DEMAND_DATE =
            new CustomError(12,"Дата продажи не указана", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError WRONG_DEMAND_COUNT =
            new CustomError(13,"Количество продажи не может быть нулевым или отрицательным", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError WRONG_DEMAND_SOLDPRICE =
            new CustomError(14,"Цена продажи не может быть нулевой или отрицательной", HttpServletResponse.SC_BAD_REQUEST);
    public static final CustomError SOLD_DATE_EXC =
            new CustomError(15,"За указанную дату можно продать только %s выбранного продукта", HttpServletResponse.SC_BAD_REQUEST);


    public static final String OK = "{ \"status\": \"OK\" }";
}
