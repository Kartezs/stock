package ru.kartezs.stock.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.RunScript;
import ru.kartezs.stock.ApplicationConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {

    private static final Logger log = LogManager.getLogger(Connector.class);

    public Connection getConnection() throws SQLException {
        String url = ApplicationConfig.getProperty(ApplicationConfig.DB_URL);
        String user = ApplicationConfig.getProperty(ApplicationConfig.DB_USER);
        String pass = ApplicationConfig.getProperty(ApplicationConfig.DB_PASS);
        log.debug(String.format("Try to get connection url: %s, user: %s, pass: %s", url, user, pass));
        Connection connection = DriverManager.getConnection(url, user, pass);
        connection.setAutoCommit(false);
        return connection;
    }

    /**
     * Init db by START_SQL_SCRIPT
     * By default script create if not exist schema STORE and tables PRODUCT, STOCK
     * @throws FileNotFoundException
     * @throws SQLException
     */
    public void init() throws FileNotFoundException, SQLException {
        log.debug("Start create db");
        try (Connection connection = getConnection()) {
            RunScript.execute(connection, new FileReader(getSqlFile()));
        }
        log.debug("Db successfully created");
    }

    File getSqlFile(){
        return new File(ApplicationConfig.getProperty(ApplicationConfig.START_SQL_SCRIPT_PATH));
    }
}
