CREATE SCHEMA IF NOT EXISTS STORE;
SET SCHEMA STORE;
create table IF NOT EXISTS product(id varchar(255), name varchar(255));
create table IF NOT EXISTS stock(id varchar(255), productId varchar(255), buyPrice DOUBLE, soldPrice DOUBLE, buyDate DATE, soldDate DATE, isSold BOOLEAN);
