package ru.kartezs.stock.db;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import ru.kartezs.stock.ApplicationConfig;

import java.io.File;
import java.io.IOException;

public class ConnectorTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testGetSqlFile_shouldUseDefaultSQLScriptFromClasspath_whenSQLScriptIsNotSet() {
        Connector connector = new Connector();

        File file = connector.getSqlFile();

        Assert.assertEquals(new File(this.getClass().getClassLoader().getResource(ApplicationConfig.DEFAULT_SQL_SCRIPT_NAME).getFile()).getPath(), file.getPath());
    }

    @Test
    public void testGetSqlFile_shouldSQLScriptFromProperties_whenSQLScriptIsSet() throws IOException {
        Connector connector = new Connector();
        File testFile = temporaryFolder.newFile("testFile.sql");
        ApplicationConfig.getInstance().getProperties().setProperty(ApplicationConfig.START_SQL_SCRIPT_PATH, testFile.getPath());

        File file = connector.getSqlFile();

        Assert.assertEquals(file.getPath(), testFile.getPath());
    }
}
