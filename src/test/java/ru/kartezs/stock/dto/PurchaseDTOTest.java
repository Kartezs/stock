package ru.kartezs.stock.dto;

import org.junit.Assert;
import org.junit.Test;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

public class PurchaseDTOTest {

    @Test
    public void testCheck_shouldThrowFrontException_whenCountWrong() {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setCount(-1);

        try {
            purchaseDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_PURCHASE_COUNT);
        }
    }

    @Test
    public void testCheck_shouldThrowFrontException_whenBuyPriceWrong() {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setBuyPrice(-1);

        try {
            purchaseDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_PURCHASE_BUYPRICE);
        }
    }

    @Test
    public void testCheck_shouldThrowFrontException_whenProductNameWrong() {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setProductName(null);
        purchaseDTO.setBuyPrice(1);

        try {
            purchaseDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_PRODUCT_NAME);
        }
    }

    @Test
    public void testCheck_shouldThrowFrontException_whenBuyDateWrong() {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setBuyPrice(1);
        purchaseDTO.setProductName("123");
        purchaseDTO.setBuyDate(null);

        try {
            purchaseDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_PURCHASE_DATE);
        }
    }
}
