package ru.kartezs.stock.dto;

import org.junit.Assert;
import org.junit.Test;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

public class SalesReportDTOTest {

    @Test
    public void testCheck_shouldThrowFrontException_whenProductNameWrong() {
        SalesReportDTO salesReportDTO = new SalesReportDTO();
        salesReportDTO.setProductName(null);

        try {
            salesReportDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_PRODUCT_NAME);
        }
    }

    @Test
    public void testCheck_shouldThrowFrontException_whenSoldPriceWrong() {
        SalesReportDTO salesReportDTO = new SalesReportDTO();
        salesReportDTO.setProductName("123");
        salesReportDTO.setDate(null);

        try {
            salesReportDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_DEMAND_DATE);
        }
    }
}
