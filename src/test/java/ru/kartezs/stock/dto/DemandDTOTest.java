package ru.kartezs.stock.dto;

import org.junit.Assert;
import org.junit.Test;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

public class DemandDTOTest {

    @Test
    public void testCheck_shouldThrowFrontException_whenCountWrong() {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setCount(-1);

        try {
            demandDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_DEMAND_COUNT);
        }
    }

    @Test
    public void testCheck_shouldThrowFrontException_whenSoldPriceWrong() {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setSoldPrice(-1);

        try {
            demandDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_DEMAND_SOLDPRICE);
        }
    }

    @Test
    public void testCheck_shouldThrowFrontException_whenProductNameWrong() {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setProductName(null);
        demandDTO.setSoldPrice(1);

        try {
            demandDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_PRODUCT_NAME);
        }
    }

    @Test
    public void testCheck_shouldThrowFrontException_whenSoldDateWrong() {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setSoldPrice(1);
        demandDTO.setProductName("123");
        demandDTO.setSoldDate(null);

        try {
            demandDTO.check();
        } catch (FrontException e) {
            Assert.assertEquals(e.getCustomError(), FrontAnswer.WRONG_DEMAND_DATE);
        }
    }
}
