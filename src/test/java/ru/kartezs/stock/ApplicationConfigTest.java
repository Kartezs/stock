package ru.kartezs.stock;

import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;


public class ApplicationConfigTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();
    private Properties defaultProperties;
    private Properties customProperties;

    @Before
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        Field instance = ApplicationConfig.class.getDeclaredField("applicationConfig");
        instance.setAccessible(true);
        instance.set(null, null);
        instance.setAccessible(false);

        defaultProperties = new Properties();
        defaultProperties.setProperty(ApplicationConfig.DB_URL, "jdbc:h2:./src/test/resources/test");
        defaultProperties.setProperty(ApplicationConfig.DB_USER, "sa");
        defaultProperties.setProperty(ApplicationConfig.DB_PASS, "");
        defaultProperties.setProperty(ApplicationConfig.SCHEMA, "store");
        defaultProperties.setProperty(ApplicationConfig.START_SQL_SCRIPT_PATH, "target/test-classes/start.sql");

        customProperties = new Properties();
        customProperties.setProperty(ApplicationConfig.DB_URL, "jdbc:h2:/home/user");
        customProperties.setProperty(ApplicationConfig.DB_USER, "user");
        customProperties.setProperty(ApplicationConfig.DB_PASS, "custom");
        customProperties.setProperty(ApplicationConfig.SCHEMA, "stock");
        customProperties.setProperty(ApplicationConfig.START_SQL_SCRIPT_PATH, "/home/user/start.sql");
    }

    @Test
    public void testProperties_shouldReturnDefaultEqualProperties_whenConfigFilePathIsNotSet(){
        ApplicationConfig applicationConfig = ApplicationConfig.getInstance();

        assertProperties(defaultProperties, applicationConfig.getProperties());
    }

    @Test
    public void testProperties_shouldReturnCustomProperties_whenConfigFilePathSet() throws IOException {
        File file = temporaryFolder.newFile("config.properties");
        customProperties.store(new FileWriter(file), null);
        System.setProperty(ApplicationConfig.CONFIG_FILE_PATH, file.getPath());

        ApplicationConfig applicationConfig = ApplicationConfig.getInstance();

        assertProperties(customProperties, applicationConfig.getProperties());
    }

    @Test
    public void testProperties_shouldReturnDefaultEqualProperties_whenCustomConfigPathIsDirectory() throws IOException {
        System.setProperty(ApplicationConfig.CONFIG_FILE_PATH, temporaryFolder.getRoot().getPath());

        ApplicationConfig applicationConfig = ApplicationConfig.getInstance();

        assertProperties(defaultProperties, applicationConfig.getProperties());
    }

    @Test
    public void testProperties_shouldReturnDefaultEqualProperties_whenCustomConfigPathIsWrong() throws IOException {
        System.setProperty(ApplicationConfig.CONFIG_FILE_PATH, temporaryFolder.getRoot().getPath() + "123");

        ApplicationConfig applicationConfig = ApplicationConfig.getInstance();

        assertProperties(defaultProperties, applicationConfig.getProperties());
    }

    private void assertProperties(Properties expectProperties, Properties actualProperties){
        Assert.assertEquals(expectProperties.getProperty(ApplicationConfig.DB_URL), actualProperties.getProperty(ApplicationConfig.DB_URL));
        Assert.assertEquals(expectProperties.getProperty(ApplicationConfig.DB_USER), actualProperties.getProperty(ApplicationConfig.DB_USER));
        Assert.assertEquals(expectProperties.getProperty(ApplicationConfig.DB_PASS), actualProperties.getProperty(ApplicationConfig.DB_PASS));
        Assert.assertEquals(expectProperties.getProperty(ApplicationConfig.SCHEMA), actualProperties.getProperty(ApplicationConfig.SCHEMA));
        Assert.assertTrue(actualProperties.getProperty(ApplicationConfig.START_SQL_SCRIPT_PATH).contains(expectProperties.getProperty(ApplicationConfig.START_SQL_SCRIPT_PATH)));
    }

}