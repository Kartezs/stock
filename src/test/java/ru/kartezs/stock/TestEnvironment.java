package ru.kartezs.stock;

import lombok.Getter;
import org.h2.Driver;
import ru.kartezs.stock.db.Connector;
import ru.kartezs.stock.servlet.ProductServletTest;

import java.io.FileNotFoundException;
import java.sql.SQLException;

public class TestEnvironment {
    @Getter
    private Connector connector;

    public void init() throws FileNotFoundException, SQLException {
        // Set test config
        System.setProperty(ApplicationConfig.CONFIG_FILE_PATH, ProductServletTest.class.getClassLoader().getResource("application.properties").getPath());
        ApplicationConfig.getInstance();
        // Update start sql script
        ApplicationConfig.getInstance().getProperties()
                .setProperty(ApplicationConfig.START_SQL_SCRIPT_PATH, ProductServletTest.class.getClassLoader().getResource("start.sql").getPath());

        // Init connector
        Driver.load();
        connector = new Connector();
        connector.init();
    }
}
