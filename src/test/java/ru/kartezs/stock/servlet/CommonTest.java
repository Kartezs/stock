package ru.kartezs.stock.servlet;

import org.junit.*;
import org.mockito.ArgumentCaptor;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.CustomError;
import ru.kartezs.stock.exceptions.FrontException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

public class CommonTest extends AbstractServletTest{

    private InfoServletTest infoServlet;
    private PurchaseServletTest purchaseServlet;
    private DemandServletTest demandServlet;
    private ProductServletTest productServlet;

    @BeforeClass
    public static void mainSetUp() throws FileNotFoundException, SQLException {
        setUpEnvironment();
    }

    @Before
    public void setUp() throws SQLException, IOException {
        infoServlet = new InfoServletTest();
        infoServlet.setUp();
        productServlet = new ProductServletTest();
        productServlet.setUp();
        demandServlet = new DemandServletTest();
        demandServlet.setUp();
        purchaseServlet = new PurchaseServletTest();
        purchaseServlet.setUp();
    }


    /**
     * Test process task example
     * @throws ServletException
     * @throws SQLException
     * @throws IOException
     */
    @Test
    public void firstProcessTest() throws ServletException, SQLException, IOException {
        executePurchase("telephone", 1, 1000, "2017-01-01");
        verify(purchaseServlet.getServlet()).getSuccessMessage();

        executePurchase("telephone", 2, 2000, "2017-02-01");
        verify(purchaseServlet.getServlet(), times(2)).getSuccessMessage();

        executeDemand("telephone", 2, 5000, "2017-03-01");
        verify(demandServlet.getServlet()).getSuccessMessage();

        executeInfo("telephone", "2017-03-01");
        Assert.assertEquals("{\"profit\": 7000.00 }", infoServlet.getServlet().getSuccessMessage());

        executeInfo("telephone", "2017-02-28");
        Assert.assertEquals("{\"profit\": 0.00 }", infoServlet.getServlet().getSuccessMessage());
    }

    /**
     * This test is check all work process
     * @throws ServletException
     * @throws IOException
     * @throws SQLException
     */
    @Test
    public void secondProcessTest() throws ServletException, IOException, SQLException {
        // Bought 10 product - 1
        executePurchase("1", 10, 10000, "2019-10-15");
        verify(purchaseServlet.getServlet()).getSuccessMessage();

        // Bought 5 product - 1
        executePurchase("1", 5, 5000, "2019-10-16");
        verify(purchaseServlet.getServlet(), times(2)).getSuccessMessage();

        // Bought 5 product - 1
        executePurchase("1", 5, 15000, "2019-10-17");
        verify(purchaseServlet.getServlet(), times(3)).getSuccessMessage();

        // Try to calculate by 2019-10-17
        executeInfo("1", "2019-10-17");
        // Result 0, because we did't sold anything
        Assert.assertEquals("{\"profit\": 0.00 }", infoServlet.getServlet().getSuccessMessage());

        // Sold 3 product - 1
        executeDemand("1", 3, 15000, "2019-10-16");
        verify(demandServlet.getServlet()).getSuccessMessage();

        // Try to calculate by 2019-10-17
        executeInfo("1", "2019-10-17");
        // Result 15000
        Assert.assertEquals("{\"profit\": 15000.00 }", infoServlet.getServlet().getSuccessMessage());

        // Try to sold don't exist product
        executeDemand("3", 3, 25000, "2019-10-16");
        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        // Catch frontException
        verify(demandServlet.getServlet(), times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        CustomError expectedError = FrontAnswer.EMPTY_PRODUCT_COUNT.updateErrorMessage("0");
        CustomError actualError = captor.getAllValues().get(0).getCustomError();
        // Assert that product is not exist
        Assert.assertEquals(expectedError, actualError);

        // Try to sold 14 product - 1
        executeDemand("1", 14, 25000, "2019-10-16");
        captor = ArgumentCaptor.forClass(FrontException.class);
        verify(demandServlet.getServlet(), times(2)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        expectedError = FrontAnswer.SOLD_DATE_EXC.updateErrorMessage("12");
        actualError = captor.getAllValues().get(1).getCustomError();
        // Assert that product 1 before 2019-10-16 is less than 14
        Assert.assertEquals(expectedError, actualError);

        // Sold 3 product - 1
        executeDemand("1", 3, 25000, "2019-10-17");
        verify(demandServlet.getServlet(), times(2)).getSuccessMessage();

        // Create new product
        executeProduct("2");
        verify(productServlet.getServlet()).getSuccessMessage();

        // Sold 10 product - 1
        executeDemand("1", 10, 25000, "2019-10-17");
        verify(demandServlet.getServlet(), times(3)).getSuccessMessage();

        // Try to calculate by 2019-10-17
        executeInfo("1", "2019-10-17");
        // Result 230000
        Assert.assertEquals("{\"profit\": 230000.00 }", infoServlet.getServlet().getSuccessMessage());


        // Try to sold 5 product - 1
        executeDemand("1", 5, 12000, "2019-10-17");
        captor = ArgumentCaptor.forClass(FrontException.class);
        verify(demandServlet.getServlet(), times(3)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        expectedError = FrontAnswer.EMPTY_PRODUCT_COUNT.updateErrorMessage("4");
        actualError = captor.getAllValues().get(2).getCustomError();
        // Not enough product
        Assert.assertEquals(expectedError, actualError);

    }

    private void executeProduct(String productName) throws ServletException, SQLException, IOException {
        String requestBody = productServlet.createProductRequestString(productName);
        doReturn(requestBody).when(productServlet.getServlet()).readRequestContent(any(HttpServletRequest.class));
        productServlet.execute();
    }

    private void executePurchase(String productName, int count, int buyPrice, String buyDate) throws IOException, ServletException, SQLException {
        String requestBody = purchaseServlet.createPurchaseRequestString(productName, count, buyPrice, buyDate);
        doReturn(requestBody).when(purchaseServlet.getServlet()).readRequestContent(any(HttpServletRequest.class));
        purchaseServlet.execute();
    }

    private void executeInfo(String productName, String date) throws ServletException, SQLException, IOException {
        String requestBody = infoServlet.createInfoRequestString(productName, date);
        doReturn(requestBody).when(infoServlet.getServlet()).readRequestContent(any(HttpServletRequest.class));
        doReturn("GET").when(infoServlet.getRequest()).getMethod();
        infoServlet.execute();
    }

    private void executeDemand(String productName, int count, int soldPrice, String soldDate) throws ServletException, SQLException, IOException {
        String requestBody = demandServlet.createDemandRequestString(productName, count, soldPrice, soldDate);
        doReturn(requestBody).when(demandServlet.getServlet()).readRequestContent(any(HttpServletRequest.class));
        demandServlet.execute();
    }

}
