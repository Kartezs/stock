package ru.kartezs.stock.servlet;

import lombok.Getter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

public class InfoServletTest extends AbstractServletTest{

    @Getter
    private InfoServlet servlet;

    @BeforeClass
    public static void mainSetUp() throws FileNotFoundException, SQLException {
        setUpEnvironment();
    }

    @Before
    public void setUp() throws SQLException, IOException {
        // Spy servlet
        servlet = spy(new InfoServlet());

        abstractSetUp(servlet);
    }

    @Test
    public void testGetInfo_shouldReturn0_whenPurchaseAndDemandNotExist() throws Exception {
        String requestBody = createInfoRequestString("134", "2019-10-10");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));
        doReturn("GET").when(request).getMethod();

        servlet.service(request, response);

        verify(servlet, times(1)).getSuccessMessage();
        Assert.assertEquals("{\"profit\": 0.00 }", servlet.getSuccessMessage());
    }

    @Test
    public void testGetInfo_shouldThrowException_whenWrongJson() throws IOException, ServletException {
        String errorRequest = "{ ";
        doReturn(errorRequest).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_REQUEST_BODY, captor.getValue().getCustomError());
    }

    @Test
    public void testGetInfo_shouldThrowException_whenWrongProductName() throws IOException, ServletException {
        String requestBody = createInfoRequestString("", "2019-10-10");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_PRODUCT_NAME, captor.getValue().getCustomError());
    }

    @Test
    public void testGetInfo_shouldThrowException_whenWrongDate() throws IOException, ServletException {
        String requestBody = createInfoRequestString("123", "2019-10");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_DATE_FORMAT, captor.getValue().getCustomError());
    }

    public String createInfoRequestString(String productName, String date){
        return String.format("{ \"productName\": \"%s\", \"date\": \"%s\"}", productName, date);
    }
}
