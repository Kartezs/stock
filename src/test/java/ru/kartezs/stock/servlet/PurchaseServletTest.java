package ru.kartezs.stock.servlet;

import lombok.Getter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

public class PurchaseServletTest extends AbstractServletTest{

    @Getter
    private  PurchaseServlet servlet;

    @BeforeClass
    public static void mainSetUp() throws FileNotFoundException, SQLException {
        setUpEnvironment();
    }

    @Before
    public void setUp() throws SQLException, IOException {
        // Spy servlet
        servlet = spy(new PurchaseServlet());

        abstractSetUp(servlet);
    }

    @Test
    public void testPurchaseSave_shouldSavePurchase_whenSetCorrectRequest() throws Exception {
        String requestBody = createPurchaseRequestString("134", 1, 1, "2019-10-15");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        verify(servlet, times(1)).getSuccessMessage();
    }

    @Test
    public void testPurchaseSave_shouldSavePurchaseAndGetExistProduct_whenSetCorrectRequest() throws Exception {
        String requestBody = createPurchaseRequestString("134", 1, 1, "2019-10-15");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        verify(servlet, times(1)).getSuccessMessage();
    }

    @Test
    public void testPurchaseSave_shouldThrowException_whenWrongJson() throws IOException, ServletException {
        String errorRequest = "{ ";
        doReturn(errorRequest).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_REQUEST_BODY, captor.getValue().getCustomError());
    }

    @Test
    public void testPurchaseSave_shouldThrowException_whenWrongProductName() throws IOException, ServletException {
        String requestBody = createPurchaseRequestString("", 1, 1, "2019-10-15");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_PRODUCT_NAME, captor.getValue().getCustomError());
    }

    @Test
    public void testPurchaseSave_shouldThrowException_whenWrongCount() throws IOException, ServletException {
        String requestBody = createPurchaseRequestString("123", -1, 1, "2019-10-15");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_PURCHASE_COUNT, captor.getValue().getCustomError());
    }

    @Test
    public void testPurchaseSave_shouldThrowException_whenWrongBuyPrice() throws IOException, ServletException {
        String requestBody = createPurchaseRequestString("123", 1, -1, "2019-10-15");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_PURCHASE_BUYPRICE, captor.getValue().getCustomError());
    }

    @Test
    public void testPurchaseSave_shouldThrowException_whenWrongDate() throws IOException, ServletException {
        String requestBody = createPurchaseRequestString("123", 1, 1, "2019--15");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_DATE_FORMAT, captor.getValue().getCustomError());
    }

    public String createPurchaseRequestString(String productName, int count, int buyPrice, String buyDate){
        return String.format("{ \"productName\": \"%s\", \"count\": %d, \"buyPrice\": %d, \"buyDate\": \"%s\"}", productName, count, buyPrice, buyDate);
    }
}
