package ru.kartezs.stock.servlet;

import lombok.Getter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

public class ProductServletTest extends AbstractServletTest{

    @Getter
    private  ProductServlet servlet;

    @BeforeClass
    public static void mainSetUp() throws FileNotFoundException, SQLException {
        setUpEnvironment();
    }

    @Before
    public void setUp() throws SQLException, IOException {
        // Spy servlet
        servlet = spy(new ProductServlet());

        abstractSetUp(servlet);
    }

    @Test
    public void testProductSave_shouldSaveProduct_whenSetCorrectRequest() throws Exception {
        String requestBody = createProductRequestString("test");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        verify(servlet, times(1)).getSuccessMessage();
    }

    /**
     * Test will pass only when run all class test
     * @throws IOException
     * @throws ServletException
     * @throws SQLException
     */
    @Test
    public void testProductSave_shouldThrowException_whenSaveExistProduct() throws IOException, ServletException, SQLException {
        String requestBody = createProductRequestString("test");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.PRODUCT_EXIST, captor.getValue().getCustomError());
    }

    @Test
    public void testProductSave_shouldThrowException_whenWrongJson() throws IOException, ServletException {
        String errorRequest = "{ \"name\": \"tes}";
        doReturn(errorRequest).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_REQUEST_BODY, captor.getValue().getCustomError());
    }

    @Test
    public void testProductSave_shouldThrowException_whenWrongMethod() throws IOException, ServletException, SQLException {
        String requestBody = createProductRequestString("test");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));
        doReturn("GET").when(request).getMethod();

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.METHOD_IS_UNSUPPORTED, captor.getValue().getCustomError());
    }

    @Test
    public void testProductSave_shouldThrowException_whenEmptyName() throws IOException, ServletException, SQLException {
        String requestBody = createProductRequestString("");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));
        doReturn("GET").when(request).getMethod();

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_PRODUCT_NAME, captor.getValue().getCustomError());
    }

    public String createProductRequestString(String productName) throws ServletException, SQLException, IOException {
        return String.format("{ \"name\": \"%s\"}", productName);
    }

}
