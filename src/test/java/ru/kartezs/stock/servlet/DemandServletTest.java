package ru.kartezs.stock.servlet;

import lombok.Getter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.exceptions.FrontException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

public class DemandServletTest extends AbstractServletTest{

    @Getter
    private  DemandServlet servlet;

    @BeforeClass
    public static void mainSetUp() throws FileNotFoundException, SQLException {
        setUpEnvironment();
    }

    @Before
    public void setUp() throws SQLException, IOException {
        // Spy servlet
        servlet = spy(new DemandServlet());

        abstractSetUp(servlet);
    }

    @Test
    public void testDemandSave_shouldThrowException_whenEmptyProductName() throws Exception {
        String requestBody = createDemandRequestString("", 5, 20000, "2019-10-16");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_PRODUCT_NAME, captor.getValue().getCustomError());
    }

    @Test
    public void testDemandSave_shouldThrowException_whenWrongJson() throws IOException, ServletException {
        String errorRequest = "{ ";
        doReturn(errorRequest).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_REQUEST_BODY, captor.getValue().getCustomError());
    }

    @Test
    public void testDemandSave_shouldThrowException_whenWrongCount() throws IOException, ServletException {
        String requestBody = createDemandRequestString("123", -1, 1, "2019-10-16");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_DEMAND_COUNT, captor.getValue().getCustomError());
    }

    @Test
    public void testDemandSave_shouldThrowException_whenWrongBuyPrice() throws IOException, ServletException {
        String requestBody = createDemandRequestString("123", 1, -1, "2019-10-16");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_DEMAND_SOLDPRICE, captor.getValue().getCustomError());
    }

    @Test
    public void testDemandSave_shouldThrowException_whenWrongDate() throws IOException, ServletException {
        String requestBody = createDemandRequestString("123", 1, -1, "2019-106");
        doReturn(requestBody).when(servlet).readRequestContent(any(HttpServletRequest.class));

        servlet.service(request, response);

        ArgumentCaptor<FrontException> captor = ArgumentCaptor.forClass(FrontException.class);
        verify(servlet, times(1)).writeFrontendExceptionToResponse(any(HttpServletResponse.class), captor.capture());
        Assert.assertEquals(FrontAnswer.WRONG_DATE_FORMAT, captor.getValue().getCustomError());
    }

    public String createDemandRequestString(String productName, int count, int soldPrice, String soldDate){
        return String.format("{ \"productName\": \"%s\", \"count\": %d, \"soldPrice\": %d, \"soldDate\": \"%s\"}", productName, count, soldPrice, soldDate);
    }

}
