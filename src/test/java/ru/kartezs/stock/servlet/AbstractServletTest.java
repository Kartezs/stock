package ru.kartezs.stock.servlet;

import lombok.Getter;
import org.h2.Driver;
import ru.kartezs.stock.ApplicationConfig;
import ru.kartezs.stock.TestEnvironment;
import ru.kartezs.stock.db.Connector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class AbstractServletTest {
    private static TestEnvironment testEnvironment;
    @Getter
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    private AbstractConnectionServlet servlet;

    protected static Connection getConnection() throws SQLException {
        return testEnvironment.getConnector().getConnection();
    }

    protected static void setUpEnvironment() throws FileNotFoundException, SQLException {
        testEnvironment = new TestEnvironment();
        testEnvironment.init();
    }

    protected void abstractSetUp(AbstractConnectionServlet servlet) throws SQLException, IOException {
        this.servlet = servlet;
        // Return test connection
        doReturn(getConnection()).when(servlet).getConnection();

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);

        // Set post method
        doReturn("POST").when(request).getMethod();
        // Set content type
        doReturn("application/json").when(request).getContentType();

        // Set writer
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(outputStream);
        doReturn(writer).when(response).getWriter();
    }

    public void execute() throws ServletException, IOException, SQLException {
        doReturn(getConnection()).when(servlet).getConnection();
        servlet.service(request, response);
    }

}
