package ru.kartezs.stock.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kartezs.stock.TestEnvironment;
import ru.kartezs.stock.domains.Product;
import ru.kartezs.stock.exceptions.FrontException;
import ru.kartezs.stock.repo.ProductRepo;
import ru.kartezs.stock.servlet.AbstractServletTest;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

public class ProductServiceTest {

    private ProductService productService;
    private ProductRepo productRepo;

    @Before
    public void setUp(){
        productRepo = mock(ProductRepo.class);
        productService = spy(new ProductService(productRepo));
    }

    @Test(expected = FrontException.class)
    public void testCreateProduct_shouldThrowFrontException_whenNameIsNull() throws SQLException, FrontException {
        productService.createProduct(null);
    }

    @Test
    public void testCreateProduct_shouldReturnNull_whenProductIsExist() throws SQLException, FrontException {
        doReturn(true).when(productRepo).isProductExist(anyString());

        Product product = productService.createProduct("test");

        Assert.assertNull(product);
    }

    @Test
    public void testCreateProduct_shouldReturnProduct_whenProductNotExist() throws SQLException, FrontException {
        Product testProduct = new Product("test", "test");
        doReturn(false).when(productRepo).isProductExist(anyString());
        doReturn(testProduct).when(productRepo).saveProduct(any(Product.class));

        Product product = productService.createProduct("test");

        Assert.assertNotNull(product);
        Assert.assertEquals(product, testProduct);
    }

    @Test(expected = FrontException.class)
    public void testGetProduct_shouldThrowFrontException_whenNameIsNull() throws SQLException, FrontException {
        productService.createProduct(null);
    }

    @Test
    public void testGetProduct_shouldReturnProductFromDB_whenProductExistInDB() throws SQLException, FrontException {
        String productName = "test";
        Product productFromDb = new Product("1", productName);
        doReturn(productFromDb).when(productRepo).getProductByName(productName);

        Product product = productService.getProduct(productName);

        Assert.assertNotNull(product);
        Assert.assertEquals(product, productFromDb);
    }

    @Test
    public void testGetProduct_shouldSaveAndReturnNewProduct_whenProductNotExistInDB() throws SQLException, FrontException {
        String productName = "test";
        Product newProduct = new Product("1", productName);
        doReturn(null).when(productRepo).getProductByName(productName);
        doReturn(newProduct).when(productRepo).saveProduct(any(Product.class));

        Product product = productService.getProduct(productName);

        Assert.assertNotNull(product);
        Assert.assertEquals(product, newProduct);
    }

    @Test
    public void testCreateNewProduct_shouldCreateProductWithDifferentId_whenCallWithSameName(){
        String testName = "test";
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            products.add(productService.createNewProduct(testName));
        }
        Set<Product> filtered = new HashSet<>(products);
        Assert.assertEquals(products.size(), filtered.size());
    }

    /**
     * Test create db to check GetProduct method
     * @throws FileNotFoundException
     * @throws SQLException
     * @throws FrontException
     */
    @Test
    public void testGetProduct_shouldReturnSameProduct_whenNameIsSame() throws FileNotFoundException, SQLException, FrontException {
        TestEnvironment testEnvironment = new TestEnvironment();
        testEnvironment.init();
        try(Connection connection = testEnvironment.getConnector().getConnection()) {
            productService = new ProductService(connection);

            Set<Product> products = new HashSet<>();
            for (int i = 0; i < 10; i++) {
                products.add(productService.getProduct("test"));
            }

            Assert.assertEquals(products.size(), 1);
        }
    }
}
