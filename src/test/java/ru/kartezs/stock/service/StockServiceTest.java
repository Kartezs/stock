package ru.kartezs.stock.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kartezs.stock.FrontAnswer;
import ru.kartezs.stock.domains.Product;
import ru.kartezs.stock.domains.Stock;
import ru.kartezs.stock.dto.DemandDTO;
import ru.kartezs.stock.dto.PurchaseDTO;
import ru.kartezs.stock.exceptions.FrontException;
import ru.kartezs.stock.repo.StockRepo;

import java.sql.SQLException;
import java.time.LocalDate;

import static org.mockito.Mockito.*;

public class StockServiceTest {

    private StockService stockService;
    private ProductService productService;
    private StockRepo stockRepo;
    private Product testProduct;
    @Before
    public void setUp() throws SQLException, FrontException {
        stockRepo = mock(StockRepo.class);
        productService = mock(ProductService.class);
        stockService = spy(new StockService(stockRepo, productService));

        testProduct = new Product("test", "test");
        doReturn(testProduct).when(productService).getProduct(anyString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateStocksFromPurchase_shouldThrowIllegalException_whenCountIsWrong() throws SQLException, FrontException {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setCount(-1);

        stockService.createStocksFromPurchase(purchaseDTO);
    }

    @Test
    public void testCreateStocksFromPurchase_shouldNotCallCreatePurchaseStock_whenCount0() throws SQLException, FrontException {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setCount(0);

        stockService.createStocksFromPurchase(purchaseDTO);

        verify(stockService, times(0)).createPurchaseStock(anyString(), any(LocalDate.class), anyDouble());
    }

    @Test
    public void testCreateStocksFromPurchase_shouldCallCreatePurchaseStock10Times_whenCount10() throws SQLException, FrontException {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setCount(10);

        stockService.createStocksFromPurchase(purchaseDTO);

        verify(stockService, times(10)).createPurchaseStock(anyString(), any(LocalDate.class), anyDouble());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateStocksFromDemand_shouldThrowIllegalException_whenCountIsWrong() throws SQLException, FrontException {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setCount(-1);

        stockService.updateStocksFromDemand(demandDTO);
    }

    @Test
    public void testUpdateStocksFromDemand_shouldNotCallCreatePurchaseStock_whenCount0() throws SQLException, FrontException {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setCount(0);

        stockService.updateStocksFromDemand(demandDTO);

        verify(stockService, times(0)).createDemandStock(anyString(), any(LocalDate.class), anyDouble());
    }

    @Test
    public void testUpdateStocksFromDemand_shouldCallCreatePurchaseStock10Times_whenCount10() throws SQLException, FrontException {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setCount(10);
        doReturn(null).when(stockRepo).updateStocksByFIFO(any(Stock.class));

        stockService.updateStocksFromDemand(demandDTO);

        verify(stockService, times(10)).createDemandStock(anyString(), any(LocalDate.class), anyDouble());
    }

    @Test
    public void testUpdateStocksFromDemand_shouldThrowFrontException_whenTryToUpdateReturnEMPTY_PRODUCT_COUNT() throws SQLException, FrontException {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setCount(10);
        doReturn(StockRepo.ExceptionType.EMPTY_PRODUCT_COUNT).when(stockRepo).updateStocksByFIFO(any(Stock.class));

        try {
            stockService.updateStocksFromDemand(demandDTO);
        } catch (FrontException e){
            Assert.assertEquals(FrontAnswer.EMPTY_PRODUCT_COUNT.updateErrorMessage("0"), e.getCustomError());
        }

    }

    @Test
    public void testUpdateStocksFromDemand_shouldThrowFrontException_whenTryToUpdateUnExistStock() throws SQLException, FrontException {
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setCount(10);
        doReturn(StockRepo.ExceptionType.SOLD_DATE_EXC).when(stockRepo).updateStocksByFIFO(any(Stock.class));

        try {
            stockService.updateStocksFromDemand(demandDTO);
        } catch (FrontException e){
            Assert.assertEquals(FrontAnswer.SOLD_DATE_EXC.updateErrorMessage("0"), e.getCustomError());
        }

    }

}
