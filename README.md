# Stock 

Api для управления складом

- [Способы взаимодействия](#cпособы-взаимодействия)
- [Список ошибок](#список-ошибок)
- [Инструкции к сборке и установке](#инструкции-к-сборке-и-установке)
- [Конфигурация](#конфигурация)

## Способы взаимодействия 

> Работа с данными происходит в JSON формате. В запросе должен быть заголовок Content-Type: application/json 

### Предоставляемые методы:

#### Создание товара
```
POST http://[host]/stock/newproduct
Пример входных значений:
{
    "name": "product1"  (Наименование товара)
}

Пример положительного ответа:
{
    "status": "OK"
}
```

#### Закупить товар
```
POST http://[host]/stock/purchase
Пример входных значений:
{ 
    "productName": "product1", (Наименование товара)
    "count": 10, (Количество товара)
    "buyPrice": 10000, (Цена за штуку)
    "buyDate": "2019-10-10" (Дата покупки. Формат: yyyy-mm-dd)
}

Пример положительного ответа:
{
    "status": "OK"
}
```

#### Продать товар
```
POST http://[host]/stock/demand
Пример входных значений:
{ 
    "productName": "product1", (Наименование товара)
    "count": 10, (Количество товара)
    "soldPrice": 10000, (Цена за штуку)
    "soldDate": "2019-10-10" (Дата продажи. Формат: yyyy-mm-dd)
}

Пример положительного ответа:
{
    "status": "OK"
}
```

#### Рассчитать прибыль
```
GET http://[host]/stock/salesreport
Пример входных значений:
{ 
    "productName": "product1", (Наименование товара)
    "date": "2019-10-10" (До указанного периода. Формат: yyyy-mm-dd)
}

Пример положительного ответа:
{
    "profit": 20000 
}
```

## Список ошибок

```json

{
  "code": 1, 
  "errorMessage": "Тип данных не поддерживается. Используйте JSON."
}
# Не указан заголовок Content-Type: application/json

{
  "code": 2, 
  "errorMessage": "Внутренняя ошибка сервера"
}

{
  "code": 3, 
  "errorMessage": "Ошибка обработки запроса. Проверьте корректность отправляемого json-a."
}

{
  "code": 4, 
  "errorMessage": "Неправильное название продукта"
}
# Не указано название

{
  "code": 5, 
  "errorMessage": "Продукт с таким именем уже существует"
}

{
  "code": 6, 
  "errorMessage": "Данный http метод не поддерживается"
}

{
  "code": 7, 
  "errorMessage": "Количество закупки не может быть нулевым или отрицательным"
}

{
  "code": 8, 
  "errorMessage": "Цена закупки не может быть нулевой или отрицательной"
}

{
  "code": 9, 
  "errorMessage": "Дата закупки не указана"
}

{
  "code": 10, 
  "errorMessage": "Продажа не возможна, недостаточно товара. Доступно %s."
}
# Это значит что продаваемого товара больше чем купленного

{
  "code": 11, 
  "errorMessage": "Не корректный формат даты. Требуемый формат даты: yyyy-mm-dd."
}

{
  "code": 12, 
  "errorMessage": "Дата продажи не указана"
}

{
  "code": 13, 
  "errorMessage": "Количество продажи не может быть нулевым или отрицательным"
}

{
  "code": 14, 
  "errorMessage": "Цена продажи не может быть нулевой или отрицательной"
}

{
  "code": 15, 
  "errorMessage": "За указанную дату можно продать только %s выбранного продукта"
}
# Эта ошибка сигнализирует о том, что в базе не хватает указанного кол-ва продукта за указанную дату. Цифра означает сколько продукта можно продать за указанную дату.

```

## Инструкции к сборке и установке 


Для работы требуется Tomcat (Скачать можно по [ссылке](https://tomcat.apache.org/download-90.cgi))

Сборка осуществляется с помощью maven командой: mvn clean package 

В результате получается war пакет, находящийся по пути: [корневая директория]/target/stock.war

Его требуется поместить по следующему пути: [директория tomcat]/webapps/stock.war

Для просмотра логов требуется поместить файл [log4j2.xml](./logs/log4j2.xml) по следующему пути: [директория tomcat]/lib (не забудьте переопределить переменную logdir в log4j2.xml файле)

Далее запустите tomcat [директория tomcat]/bin/startup.sh (.bat)

Приложение стартанет по адресу http://localhost:8080/[название war пакета]


## Конфигурация

Хранение данных происходит в H2 базе данных.
Конфигурация задается через конфигурационный файл (для примера посмотрите на [дефолтный конфиг](./src/main/resources/application.properties)). 
Путь к файлу требуется указывать при старте опцией -Dconfig.path=[путь к файлу]. 
Если приложение стартует в tomcat, то данную опцию требуется прописать в файле [директория tomcat]/bin/setenv.sh (если файла нет то его нужно создать)
Пример: export JAVA_OPTS="-Dconfig.path=/home/user..."

В случае отсутствия данной опции приложение запустится с дефолтными настройками

Список параметров конфигурации (значение указаны дефолтные):

- db.url = jdbc:h2:./default (путь к h2 базе данных, по дефолту файл появится в директории [директория tomcat]/bin)
- db.user = sa
- db.pass =
- db.schema = store (название схемы в db)
- start.sql.path = (путь к файлу создания db, по дефолту отсутствует)

При дефолтной конфигурации файл с базой данных будет находится в директории, в которой происходил старт ([директория tomcat]/bin)
